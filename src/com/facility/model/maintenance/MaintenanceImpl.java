package com.facility.model.maintenance;

import java.util.ArrayList;
import java.util.List;

public class MaintenanceImpl implements Maintanence {

	private int id;
	
	private List<MaintenanceLogImpl>      logs;
	private List<MaintenanceScheduleImpl> schedules;
	private List<MaintenanceRequestImpl>  requests;
	private List<MaintenanceOrderImpl  >  orders;
	
	public MaintenanceImpl() {
		logs      = new ArrayList<>();
		schedules = new ArrayList<>();
		requests  = new ArrayList<>();
		orders    = new ArrayList<>();
	}

	
	public int getId() {
		return id;
	}
	
	
	public void setId(int id) {
		this.id = id;
	}
	

	public List<MaintenanceRequestImpl> getRequests() {
		return requests;
	}
	
	
	public void setRequests(List<MaintenanceRequestImpl> requests) {
		this.requests = requests;
	}
	
	
	public List<MaintenanceOrderImpl> getOrders() {
		return orders;
	}
	
	//TODO: change to add
	public void setOrders(List<MaintenanceOrderImpl> orders) {
		this.orders = orders;
	}
	
}
