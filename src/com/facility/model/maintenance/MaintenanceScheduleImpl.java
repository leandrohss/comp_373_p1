package com.facility.model.maintenance;

import java.time.LocalDateTime;

public class MaintenanceScheduleImpl {
	
	private int id;
	
	private LocalDateTime maintStartDate;
	private LocalDateTime maintEndDate;

	
	public MaintenanceScheduleImpl(LocalDateTime maintStartDate, LocalDateTime maintEndDate) {
		this.maintStartDate = maintStartDate;
		this.maintEndDate = maintEndDate;
	}


	public LocalDateTime getMaintStartDate() {
		return maintStartDate;
	}


	public void setMaintStartDate(LocalDateTime maintStartDate) {
		this.maintStartDate = maintStartDate;
	}


	public LocalDateTime getMaintEndDate() {
		return maintEndDate;
	}


	public void setMaintEndDate(LocalDateTime maintEndDate) {
		this.maintEndDate = maintEndDate;
	}


	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}
	
}
