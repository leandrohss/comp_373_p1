package com.facility.model.maintenance;

public interface MaintenanceRequest {

	public int getID();
	
	public String getProblem();
	public void setProblem(String newProblem);
	
}