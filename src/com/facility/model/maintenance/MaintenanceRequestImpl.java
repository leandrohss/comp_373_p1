package com.facility.model.maintenance;

public class MaintenanceRequestImpl implements MaintenanceRequest{

	private int id;
	
	private String problem;
	
	public MaintenanceRequestImpl(String problem) {
		this.problem = problem;
	}
	
	public MaintenanceRequestImpl(String problem, int id) {
		this.problem = problem;
		this.id = id;
	}
	
	@Override
	public void setProblem(String problem) {
		this.problem = problem;
	}


	@Override
	public String getProblem() {
		return problem;
	}


	@Override
	public int getID() {
		return id;
	}

}