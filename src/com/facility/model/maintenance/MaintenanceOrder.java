package com.facility.model.maintenance;

import java.sql.Date;

public interface MaintenanceOrder {

	public int getId();
	public void setId(int id);
	public double getCost();
	public void setCost(double cost);
	public Date getRepairStart();
	public void setRepairStart(Date repairStart);
	public Date getRepairEnd();
	public void setRepairEnd(Date repairEnd);
	
	
}
