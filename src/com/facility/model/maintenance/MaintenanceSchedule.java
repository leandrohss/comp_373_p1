package com.facility.model.maintenance;

import java.time.LocalDateTime;

public interface MaintenanceSchedule {

	public LocalDateTime getStartTime();
	public void setStartTime(LocalDateTime maintStartDate);

	public LocalDateTime getEndTime();
	public void setEndTime(LocalDateTime maintEndDate);
	
}
