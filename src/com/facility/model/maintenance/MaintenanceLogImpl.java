package com.facility.model.maintenance;

public class MaintenanceLogImpl {
	
	private int id;
	
	private String log;
	
	public MaintenanceLogImpl(String log) {
		this.log = log;
	}
	
	public String getLog() {
		return log;
	}
	
	public int getID() {
		return id;
	}
}
