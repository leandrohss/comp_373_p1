package com.facility.model.services;

import java.sql.Date;
import java.util.List;

import com.facility.dal.FacilityUseDao;
import com.facility.model.use.FacilityUse;
import com.facility.model.use.Inspection;

public class FacilityUseServiceImpl implements FacilityUseService {

	@Override
	public boolean isInUseDuringInterval(int facilityID, Date startDate, Date endDate) {

		boolean result = false;

		List<FacilityUse> facilityUses = new FacilityUseDao().getUseIntervals(facilityID);
		
		for (FacilityUse fu : facilityUses) {
			
			boolean startTimeIsBetweenSchedule =
					startDate.getTime() >= fu.getStartTime().getTime() &&
					startDate.getTime() <= fu.getEndTime().getTime();
					
			boolean endTimeIsBetweenSchedule = 
					endDate.getTime() >= fu.getStartTime().getTime() &&
					endDate.getTime() <= fu.getEndTime().getTime();
			
					
			if (startTimeIsBetweenSchedule || endTimeIsBetweenSchedule)
				result = true;
		}
		
		return result;
	}

	/**
	 * Creates a FacilityUse for a Facility in the database
	 */
	@Override
	public int assignFacilityToUse(FacilityUse facilityUse, int facilityID) {
		return new FacilityUseDao().assignFacilityToUse(facilityUse, facilityID);
	}

	
	/**
	 * Alter the status active to false
	 */
	@Override
	public int vacateFacility(int facilityID) {
		return new FacilityUseDao().vacateFacility(facilityID);
	}

	@Override
	public List<Inspection> listInspections(int facilityID) {
		return new FacilityUseDao().listInspections(facilityID);
	}

	
	/**
	 * Returns a list of {@link FacilityUse}
	 */
	@Override
	public List<FacilityUse> listActualUsage(int facilityID) {
		return new FacilityUseDao().listActualUsage(facilityID);
	}

	
	/**
	 * Returns a list the usage rate of a facility by calculating actives/disables status
	 */
	@Override
	public double calcUsageRate(int facilityID) {
		return new FacilityUseDao().calcUsageRate(facilityID);
	}

}
