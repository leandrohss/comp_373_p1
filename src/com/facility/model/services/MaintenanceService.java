package com.facility.model.services;

import java.util.List;

import com.facility.model.maintenance.MaintenanceOrder;
import com.facility.model.maintenance.MaintenanceRequest;
import com.facility.model.maintenance.MaintenanceOrderImpl;

public interface MaintenanceService{	

	public Object makeFacilityMaintRequest(MaintenanceRequest maintanceRequest, int facilityID);
	
	// IMaintenanceSchedule, IMaintenanceOrder
	public Object scheduleMaintenance(MaintenanceOrderImpl order, int facilityID);
	
	public double calcMaintenanceCostForFacility(int facilityID);
	
	public int calcProblemRateForFacility(int facilityID);
	
	public int calcDownTimeForFacility(int facilityID);
	
	public Object listMaintRequests(int facilityID);
	
	public List<MaintenanceOrder> listMaintenance(int facilityID);
	
	public List<String> listFacilityProblems(int facilityID);
	
}