package com.facility.model.services;

import java.sql.Date;
import java.util.List;

import com.facility.model.use.FacilityUse;
import com.facility.model.use.Inspection;

public interface FacilityUseService {
	
	public boolean isInUseDuringInterval(int facilityID, Date startDate, Date endDate);
	
	public int assignFacilityToUse(FacilityUse facilityUse, int facilityID);
	
	public int vacateFacility(int facilityUseID);
	
	public List<Inspection> listInspections(int facilityID);
	
	public List<FacilityUse> listActualUsage(int facilityID);
	
	public double calcUsageRate(int facilityID);

}
