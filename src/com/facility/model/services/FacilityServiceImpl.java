package com.facility.model.services;

import java.util.List;

import com.facility.dal.FacilityDAO;
import com.facility.model.facility.Facility;
import com.facility.model.facility.FacilityDetail;

public class FacilityServiceImpl implements FacilityService {

	@Override
	public List<Facility> listFacilities() {
		return new FacilityDAO().findAllFacilities();
	}

	@Override
	public FacilityDetail getFacilityInformation(int facilityID) {
		return new FacilityDAO().getFacilityDetail(facilityID);
	}

	@Override
	public int requestAvailableCapacity(int facilityID) {
		return new FacilityDAO().requestAvailableCapacity(facilityID);
	}

	@Override
	public int addNewFacility(Facility facility) {
		return new FacilityDAO().addFacility(facility);
	}

	@Override
	public void addFacilityDetail(FacilityDetail detail) {
		new FacilityDAO().addFacilityDetail(detail);
	}

	@Override
	public int removeFacility(int facilityID) {
		return new FacilityDAO().removeFacility(facilityID);
	}

}
