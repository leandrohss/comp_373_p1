package com.facility.model.services;

import java.util.List;

import com.facility.dal.MaintenanceOrderDao;
import com.facility.dal.MaintenanceRequestDao;
import com.facility.dal.MaintenanceScheduleDao;
import com.facility.model.maintenance.MaintenanceOrder;
import com.facility.model.maintenance.MaintenanceRequest;
import com.facility.model.maintenance.MaintenanceOrderImpl;

public class MaintenanceServiceImpl implements MaintenanceService {

	
	/**
	 * Creates a Maintenance to a Facility in the system.
	 */
	@Override
	public Object makeFacilityMaintRequest(MaintenanceRequest maintanceRequest, int facilityID) {
		
		MaintenanceRequestDao dao = new MaintenanceRequestDao();
		dao.save(maintanceRequest, facilityID);

		return null;
	}

	
	/**
	 * Schedule a Maintenance in the system means registering a MaintenanceOrder for a Facility in a specified time
	 */
	@Override
	public Object scheduleMaintenance(MaintenanceOrderImpl order, int facilityID) {
		
		MaintenanceScheduleDao dao = new MaintenanceScheduleDao();
		dao.save(order, facilityID);

		return null;
	}
	
	
	/**
	 * Calculate the sum of all Maintenance registered
	 */
	@Override
	public double calcMaintenanceCostForFacility(int facilityID) {

		MaintenanceOrderDao dao = new MaintenanceOrderDao();
		return dao.getCostForFacility(facilityID);
	}

	
	/**
	 * Calculate the total of Maintenance registered for a Facility
	 */
	@Override
	public int calcProblemRateForFacility(int facilityID) {

		MaintenanceRequestDao dao = new MaintenanceRequestDao();
		return dao.getProbleRateForFacility(facilityID);
	}

	
	/**
	 * Return the down time for a Facility in HOURS
	 */
	@Override
	public int calcDownTimeForFacility(int facilityID) {
		MaintenanceOrderDao dao = new MaintenanceOrderDao();
		return dao.getDownTimeForFacility(facilityID);
	}

	
	/**
	 * Return a list with all requests made for a Facility
	 */
	@Override
	public List<MaintenanceRequest> listMaintRequests(int facilityID) {
		
		MaintenanceRequestDao dao = new  MaintenanceRequestDao();
		return dao.findMaintenanceRequestsForFacility(facilityID);
	}

	/**
	 * Return a list with all maintenances ordered for a Facility
	 */
	@Override
	public List<MaintenanceOrder> listMaintenance(int facilityID) {

		MaintenanceOrderDao dao = new MaintenanceOrderDao();
		return dao.findMaintenanceOrdersForFacility(facilityID);
	
	}

	
	/**
	 * Return a list with all facility problems
	 */
	@Override
	public List<String> listFacilityProblems(int facilityID) {
		
		MaintenanceRequestDao dao = new  MaintenanceRequestDao();
		return dao.getProblemsForFacility(facilityID);
	}
	
}
