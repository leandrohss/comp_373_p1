package com.facility.model.services;

import java.util.List;

import com.facility.model.facility.Facility;
import com.facility.model.facility.FacilityDetail;

public interface FacilityService {

	public List<Facility> listFacilities();
	
	public FacilityDetail getFacilityInformation(int facilityID);
	
	public int requestAvailableCapacity(int facilityID);
	
	public int addNewFacility(Facility facility);
	
	public void addFacilityDetail(FacilityDetail detail);
	
	public int removeFacility(int facilityID);
	
}
