package com.facility.model.facility;

import java.util.List;

import com.facility.model.maintenance.Maintanence;
import com.facility.model.use.FacilityUse;
import com.facility.model.use.Inspection;

public class FacilityImpl implements Facility {

	private int id;
	
	private FacilityDetail detail;
	
	private Maintanence maintanence;
	
	private List<FacilityUse> uses;
	
	private List<Inspection> inspections;
	
	private Owner owner;
	
	private List<Client> clients;
	
	public FacilityImpl(int id) {
		this.id = id;
	}
	
	@Override
	public int getID() {
		return id;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public FacilityDetail getDetail() {
		return detail;
	}

	public void setDetail(FacilityDetail detail) {
		this.detail = detail;
	}

	public Maintanence getMaintanence() {
		return maintanence;
	}

	public void setMaintanence(Maintanence maintanence) {
		this.maintanence = maintanence;
	}

	public List<FacilityUse> getUses() {
		return uses;
	}

	public void setUses(List<FacilityUse> uses) {
		this.uses = uses;
	}

	public List<Inspection> getInspections() {
		return inspections;
	}

	public void setInspections(List<Inspection> inspections) {
		this.inspections = inspections;
	}

	public Owner getOwner() {
		return owner;
	}

	public void setOwner(Owner owner) {
		this.owner = owner;
	}

	public List<Client> getClients() {
		return clients;
	}

	public void setClients(List<Client> clients) {
		this.clients = clients;
	}
	
	public void addClient(Client client) {
		clients.add(client);
	}
	
}
