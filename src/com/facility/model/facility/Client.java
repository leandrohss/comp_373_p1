package com.facility.model.facility;

import com.facility.common.Address;
import com.facility.common.Phone;

public interface Client {

	public Address getAddress();
	
	public void setAddress(Address address);
	
	public Phone getPhone();
	
	public void setPhone(Phone phone);
	
}
