package com.facility.model.facility;

public class FacilityDetailImpl implements FacilityDetail {

	private int id;
	
	private int number;
	private int capacity;
	private int status;
	
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setNumber(int number) {
		this.number = number;
	}

	public void setCapacity(int capacity) {
		this.capacity = capacity;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public FacilityDetailImpl(int id, int number, int capacity, int status) {
		this.id = id;
		this.number = number;
		this.capacity = capacity;
		this.status = status;
	}
	
	@Override
	public int getNumber() {
		return number;
	}

	@Override
	public int getCapacity() {
		return capacity;
	}

	@Override
	public int getStatus() {
		return status;
	}
	
	public int getID() {
		return id;
	}

}
