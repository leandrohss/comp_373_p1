package com.facility.model.facility;

import java.util.List;

import com.facility.model.maintenance.Maintanence;
import com.facility.model.use.FacilityUse;
import com.facility.model.use.Inspection;

public interface Facility {
	
	public int getID();
	public void setId(int id);

	public FacilityDetail getDetail();
	public void setDetail(FacilityDetail detail);

	public Maintanence getMaintanence();
	public void setMaintanence(Maintanence maintanence);

	public List<FacilityUse> getUses();
	public void setUses(List<FacilityUse> uses);

	public List<Inspection> getInspections();
	public void setInspections(List<Inspection> inspections);

}
