package com.facility.model.facility;

public interface FacilityDetail {

	public int getNumber();
	
	public int getCapacity();
	
	public int getStatus();
	
}
