package com.facility.model.use;

public interface Inspection {

	public int getType();
	
	public int getID();
	
}
