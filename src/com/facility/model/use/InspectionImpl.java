package com.facility.model.use;

public class InspectionImpl implements Inspection {

	private int id;
	
	private int type;
	
	public InspectionImpl(int id, int type) {
		this.id = id;
		this.type = type;
	}

	@Override
	public int getType() {
		return type;
	}

	@Override
	public int getID() {
		return id;
	}
	
}
