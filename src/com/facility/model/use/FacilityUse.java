package com.facility.model.use;

import java.sql.Date;

public interface FacilityUse {

	public User getUser();
	
	public Date getStartTime();
	
	public Date getEndTime();
	
	
	public void setUser(User user);
	
	public void setStartTime(Date startTime);
	
	public void setEndTime(Date endTime);
}
