package com.facility.model.use;

import java.sql.Date;

public class FacilityUseImpl implements FacilityUse {

	private int id;
	
	Date startTime;
	Date endTime;
	
	User user;
	
	boolean active;

	public FacilityUseImpl(int id, Date startTime, Date endTime, User user, boolean active) {
		this.id = id;
		this.startTime = startTime;
		this.endTime = endTime;
		this.user = user;
		this.active = active;
	}
	
	
	@Override
	public User getUser() {
		return user;
	}


	@Override
	public Date getStartTime() {
		return startTime;
	}


	@Override
	public Date getEndTime() {
		return endTime;
	}


	public boolean isActive() {
		return active;
	}


	public void setActive(boolean active) {
		this.active = active;
	}


	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}


	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}


	public void setUser(User user) {
		this.user = user;
	}
	
	
}
