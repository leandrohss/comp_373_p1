package com.facility.model.use;

public interface User {

	public int getID();
	
	public String getFirstName();
	public String getMiddleName();
	public String getLastName();

	public void setFirstName(String firstName);
	public void setMiddleName(String middleName);
	public void setLastName(String lastName);
	
}
