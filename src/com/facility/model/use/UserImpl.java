package com.facility.model.use;

public class UserImpl implements User {

	private int id;
	
	private String firstName;
	private String middleName;
	private String lastName;
	
	public UserImpl(int id) {
		this.id = id;
	}

	@Override
	public int getID() {
		return id;
	}

	@Override
	public String getFirstName() {
		return firstName;
	}

	@Override
	public String getMiddleName() {
		return middleName;
	}

	@Override
	public String getLastName() {
		return lastName;
	}

	@Override
	public void setFirstName(String firstName) {
		this.firstName= firstName;
	}

	@Override
	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	@Override
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

}
