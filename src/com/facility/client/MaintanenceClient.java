package com.facility.client;

import java.sql.Date;
import java.util.List;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.facility.model.maintenance.MaintenanceOrder;
import com.facility.model.maintenance.MaintenanceRequest;
import com.facility.model.maintenance.MaintenanceOrderImpl;
import com.facility.model.maintenance.MaintenanceRequestImpl;
import com.facility.model.services.MaintenanceServiceImpl;

public class MaintanenceClient {
	
	public static void main(String[] arg){

		ApplicationContext context = new ClassPathXmlApplicationContext("META-INF/app-context.xml");
		System.out.println("***************** Application Context instantiated! ******************");

		//Spring to inject the right object implementation in CustomerService for customer using Setter Injection
		//Also, bootstrapping the CustomerService instantiation using factory
		MaintenanceServiceImpl maintenanceService = (MaintenanceServiceImpl) context.getBean("maintenanceService");
		System.out.println("*************** Creating service object *************************"); 

		
		System.out.println("*************** TEST makeFacilityMaintRequest *************************");
		MaintenanceRequestImpl request = (MaintenanceRequestImpl) context.getBean("maintenanceRequest");
		request.setProblem("Test problem");
		
		maintenanceService.makeFacilityMaintRequest(request, 1);
		
		System.out.println("*************** TEST scheduleMaintenance *************************");
		MaintenanceOrderImpl order = (MaintenanceOrderImpl) context.getBean("maintenanceOrder");
		order.setCost(10.0);
		order.setRepairStart(new Date(1000000));
		order.setRepairEnd(new Date(1000001));

		maintenanceService.scheduleMaintenance(order, 1);
		

		System.out.println("*************** TEST calcDownTimeForFacility *************************");
		int downTime = maintenanceService.calcDownTimeForFacility(1);
		
		System.out.println("Down time: " + downTime);
		
		System.out.println("*************** TEST calcMaintenanceCostForFacility *************************");
		double maintenanceCost = maintenanceService.calcMaintenanceCostForFacility(1);
		
		System.out.println("Maintenance cost: " + maintenanceCost);
				
		System.out.println("*************** TEST calcProblemRateForFacility *************************");
		int problemRate = maintenanceService.calcProblemRateForFacility(1);
		
		System.out.println("Problem rate: " + problemRate);
		
		System.out.println("*************** TEST listFacilityProblems *************************");
		List<String> problems = maintenanceService.listFacilityProblems(1);
		
		for (String p : problems){
			System.out.println(p);
		}
		
		System.out.println("*************** TEST listMaintenance *************************");
		List<MaintenanceOrder> orders = maintenanceService.listMaintenance(0); 

		for (MaintenanceOrder o : orders) {
			System.out.println("Order cost: " + o.getCost());
			System.out.println("Order id: " + o.getId());
			System.out.println("Order start repair: " + o.getRepairStart());
			System.out.println("Order end repair: " + o.getRepairEnd());
		}
		
		System.out.println("*************** TEST listMaintRequests *************************");
		List<MaintenanceRequest> requests = maintenanceService.listMaintRequests(1);
		
		for (MaintenanceRequest r : requests) {
			System.out.println("Order cost: " + r.getID());
			System.out.println("Order id: " + r.getProblem());
		}
	}

}
