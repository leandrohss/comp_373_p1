package com.facility.client;

import java.sql.Date;
import java.util.List;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.facility.model.services.FacilityUseServiceImpl;
import com.facility.model.use.FacilityUse;
import com.facility.model.use.Inspection;
import com.facility.model.use.User;

public class FacilityUseClient {
	
	public static void main(String[] arg){

		ApplicationContext context = new ClassPathXmlApplicationContext("META-INF/app-context.xml");
		System.out.println("***************** Application Context instantiated! ******************");

		//Spring to inject the right object implementation in CustomerService for customer using Setter Injection
		//Also, bootstrapping the CustomerService instantiation using factory
		FacilityUseServiceImpl facilityUseService = (FacilityUseServiceImpl) context.getBean("facilityUseService");
		System.out.println("*************** Creating service object *************************"); 
    
		System.out.println("*************** TEST assignFacilityToUse *************************");

		// Create objects for a FacilityUse
		
		// User
		User user = (User) context.getBean("user");
		user.setFirstName("Joseph");
		user.setMiddleName("Herrera");
		user.setLastName("Catotal");
		
		FacilityUse facilityUse = (FacilityUse) context.getBean("facilityUse");
		facilityUse.setUser(user);
		facilityUse.setStartTime(new Date(10000000));
		facilityUse.setEndTime  (new Date(10000001));
		
		// Test method
		facilityUseService.assignFacilityToUse(facilityUse, 1);

		
		System.out.println("*************** TEST calcUsageRate *************************");
		
		double usageRate = facilityUseService.calcUsageRate(1); // ID auto-populated
		
		System.out.println("Usage rate: " + usageRate);
		

		System.out.println("*************** TEST isInUseDuringInterval *************************");
		
		boolean isUsed = facilityUseService.isInUseDuringInterval(1, new Date(10000000), new Date(10000001));
		System.out.println("The facility is occupied? " + isUsed);
		
		
		
		System.out.println("*************** TEST listActualUsage *************************");
		
		List<FacilityUse> uses = facilityUseService.listActualUsage(1);
		
		for (FacilityUse u : uses) {
			System.out.println("User: " + u.getUser().getFirstName());
			System.out.println("Start time: " + u.getStartTime().toString());
			System.out.println("End time: " + u.getEndTime().toString());
		}
		
		
		System.out.println("*************** TEST listInspections *************************");
		
		List<Inspection> inspections = facilityUseService.listInspections(1);
		
		for (Inspection i : inspections) {
			System.out.println("Inspection ID: " + i.getID());
			System.out.println("Inspection Type: " + i.getType());
		}
		
		
		System.out.println("*************** TEST vacateFacility *************************");
		
		int result = facilityUseService.vacateFacility(1);
		
		if (result != 0)
			System.out.println("Facililty vacated");
		else
			System.out.println("Error: Facililty couldn't be vacated");
	}

}
