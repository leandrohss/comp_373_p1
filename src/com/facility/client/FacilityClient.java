package com.facility.client;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.facility.model.facility.FacilityImpl;
import com.facility.model.facility.Owner;
import com.facility.model.facility.FacilityDetailImpl;
import com.facility.common.Address;
import com.facility.common.Phone;
import com.facility.model.facility.Client;
import com.facility.model.facility.Facility;
import com.facility.model.facility.FacilityDetail;
import com.facility.model.maintenance.MaintenanceImpl;
import com.facility.model.maintenance.MaintenanceOrderImpl;
import com.facility.model.maintenance.MaintenanceRequestImpl;
import com.facility.model.services.FacilityServiceImpl;
import com.facility.model.use.FacilityUseImpl;
import com.facility.model.use.FacilityUse;
import com.facility.model.use.Inspection;
import com.facility.model.use.User;
import com.facility.model.use.InspectionImpl;

public class FacilityClient {

	public static void main(String[] arg){

		ApplicationContext context = new ClassPathXmlApplicationContext("META-INF/app-context.xml");
		System.out.println("***************** Application Context instantiated! ******************");

		//Spring to inject the right object implementation in CustomerService for customer using Setter Injection
		//Also, bootstrapping the CustomerService instantiation using factory
		FacilityServiceImpl facilityService = (FacilityServiceImpl) context.getBean("facilityService");
		System.out.println("*************** Creating service object *************************"); 
    

		
		System.out.println("*************** TEST addFacilityDetail *************************");

		
		FacilityDetailImpl facilityDetail = (FacilityDetailImpl) context.getBean("facilityDetail");
		facilityDetail.setCapacity(50);
		facilityDetail.setNumber(101);
		facilityDetail.setStatus(1);

		facilityService.addFacilityDetail(facilityDetail);
		
		
		System.out.println("*************** TEST addNewFacility *************************");
		
		// Create objects for a Facility
		
		// Inspection
		InspectionImpl inspection = (InspectionImpl) context.getBean("Inspection");
		List<Inspection> inspections = new ArrayList<>();
		inspections.add(inspection);
		
		// Maintenance Order
		MaintenanceOrderImpl order = (MaintenanceOrderImpl) context.getBean("maintenanceOrder");
		order.setCost(100.0);
		order.setRepairEnd(new Date(10000000));
		order.setRepairStart(new Date(10000001));
		List<MaintenanceOrderImpl> orders = new ArrayList<>();
		orders.add(order);
		
		// Maintenance Request
		MaintenanceRequestImpl maintenanceRequest = (MaintenanceRequestImpl) context.getBean("maintenanceRequest");
		maintenanceRequest.setProblem("Test problem");
		List<MaintenanceRequestImpl> requests = new ArrayList<>();
		requests.add(maintenanceRequest);
		
		// Maintanance 
		MaintenanceImpl maintenance = (MaintenanceImpl) context.getBean("maintenance");
		maintenance.setOrders(orders);
		maintenance.setRequests(requests);
		
		// User
		User user = (User) context.getBean("user");
		user.setFirstName("Joseph");
		user.setMiddleName("Herrera");
		user.setLastName("Catotal");
		
		// Facility Use
		FacilityUseImpl use = (FacilityUseImpl) context.getBean("facilityUse");
		use.setActive(true);
		use.setStartTime(new Date(10000000));
		use.setEndTime  (new Date(10000001));
		use.setUser(user);
		List<FacilityUse> uses = new ArrayList<>();
		uses.add(use);
		
		// Clients
		Client client = (Client) context.getBean("client");
		client.setAddress(new Address());
		client.setPhone(new Phone());
		List<Client> clients = new ArrayList<>();
		clients.add(client);
		
		//Owner
		Owner owner = (Owner) context.getBean("owner");
		owner.setAddress(new Address());
		owner.setPhone(new Phone());
		
		// Facility
		FacilityImpl facility = (FacilityImpl) context.getBean("facility");
		facility.setDetail(facilityDetail);
		facility.setInspections(inspections);
		facility.setMaintanence(maintenance);
		facility.setUses(uses);
		facility.addClient(client);
		facility.setOwner(owner);
		
		// Test method
		facilityService.addNewFacility(facility);
		
		
		
		System.out.println("*************** TEST getFacilityInformation *************************");

		FacilityDetail detail = facilityService.getFacilityInformation(1);
		
		System.out.println("Capacity: " + detail.getCapacity());
		System.out.println("Capacity: " + detail.getNumber());
		System.out.println("Capacity: " + detail.getStatus());

		
		
		System.out.println("*************** TEST listFacilities *************************");

		List<Facility> facilities = facilityService.listFacilities();
		
		for (Facility f : facilities) {
			System.out.println("ID: " + f.getID());
			System.out.println("Detail: " + f.getDetail().toString());
			System.out.println("inspection: " + f.getInspections().get(0).toString());
			System.out.println("Maintanence: " + f.getMaintanence().toString());
			System.out.println("Uses: " + f.getUses().get(0).toString());
		}
		
		
		System.out.println("*************** TEST removeFacility *************************");
		
		facilityService.removeFacility(1);
		
		boolean removed = true;
		for (Facility f : facilities) {
			if (f.getID() == 1)
				removed = false;
		}
		
		System.out.println("Was facility removed? " + removed);
		
		
		System.out.println("*************** TEST requestAvailableCapacity *************************");
		
		int capacity = facilityService.requestAvailableCapacity(1);
		
		System.out.println("The capacity is " + capacity);
		
	}
}
