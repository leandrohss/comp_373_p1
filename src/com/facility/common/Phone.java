package com.facility.common;

public class Phone {

	private String homeNumber;
	private String professionalNumber;

	public String getHomeNumber() {
		return homeNumber;
	}

	public void setHomeNumber(String homeNumber) {
		this.homeNumber = homeNumber;
	}

	public String getProfessionalNumber() {
		return professionalNumber;
	}

	public void setProfessionalNumber(String professionalNumber) {
		this.professionalNumber = professionalNumber;
	}

}
