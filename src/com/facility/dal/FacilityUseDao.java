package com.facility.dal;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;

import com.facility.model.use.FacilityUse;
import com.facility.model.use.Inspection;

public class FacilityUseDao {

	public List<FacilityUse> getUseIntervals(int facilityID) {
		List<FacilityUse> facilityUses = null;
		
		try {
			System.out.println("*************** Searcing for Use Intervals with ID ...  " + facilityID);
			Session session = HibernatePGSQLHelper.getSessionFactory().getCurrentSession();
			session.beginTransaction();
	        
	        Query getAddresstQuery = session.createQuery("Select FU.startTime, FU.endTime From FacilityUseImpl FU where facility_id=:facility_id");		
	        getAddresstQuery.setString("facility_id", String.valueOf(facilityID));
			
	        facilityUses = getAddresstQuery.list();
			session.getTransaction().commit();
			
		} catch (Exception e) {
			e.printStackTrace();
		}

		return facilityUses;
	}
	
	public int vacateFacility(int facilityID) {
		int result = 0;

		System.out.println("*************** Vacating Facility with ID ... " + facilityID);
		Session session = HibernatePGSQLHelper.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		
		Query getAddresstQuery = session.createQuery("update FacilityUseImpl set active = false" +
				" where facility_use_id = " + facilityID);

		result = getAddresstQuery.executeUpdate();
		session.getTransaction().commit();

		return result;
	}
	
	
	public int assignFacilityToUse(FacilityUse facilityUse, int facilityID) {
		
		System.out.println("*************** Adding facility use in DB with ID ...  " + facilityID);
		Session session = HibernatePGSQLHelper.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		session.save(facilityUse);
		session.getTransaction().commit();
		
		return 0; //success
	}
	
	public List<FacilityUse> listActualUsage(int facilityID) {
		System.out.println("*************** Searcing for all facility uses...  ");
		Session session = HibernatePGSQLHelper.getSessionFactory().getCurrentSession();
		session.beginTransaction();
	
        Query getAddresstQuery = session.createQuery("From FacilityUseImpl");		
		
		System.out.println("*************** Retrieve Query is ....>>\n" + getAddresstQuery.toString()); 
		session.getTransaction().commit();
		
		List<FacilityUse> facilityUses = getAddresstQuery.list();
		System.out.println("Getting Facailities using HQL. \n");
		
		return facilityUses;
	}
	
	public List<Inspection> listInspections(int facilityID) {
		
		System.out.println("*************** Searcing for all inpesction for the facility with ID ... " + facilityID);
		Session session = HibernatePGSQLHelper.getSessionFactory().getCurrentSession();
		session.beginTransaction();
	
        Query getAddresstQuery = session.createQuery("From InspectionImpl");		
		
		System.out.println("*************** Retrieve Query is ....>>\n" + getAddresstQuery.toString()); 
		session.getTransaction().commit();
		
		List<Inspection> inspections = getAddresstQuery.list();
		System.out.println("Getting Facailities using HQL. \n");
		
		return inspections;
	}
	

	public double calcUsageRate(int facilityID) {
		
		System.out.println("*************** Calculating usage for the facility with ID ... " + facilityID);
		Session session = HibernatePGSQLHelper.getSessionFactory().getCurrentSession();
		session.beginTransaction();
	
        Query getAddresstQuery1 = session.createQuery("SELECT count From FacilityUseImpl where facility_id = " + facilityID
				+ " active = true");
        
        int actives = (int) getAddresstQuery1.list().get(0);
        session.getTransaction().commit();
        
        Query getAddresstQuery2 = session.createQuery("SELECT count From FacilityUseImpl where facility_id = " + facilityID
				+ " active = true");
        
        int disables = (int) getAddresstQuery2.list().get(0);
        session.getTransaction().commit();
        
        double usage = (disables != 0) ? actives/(double)disables : 1;
        
        return usage;
	}
	
}
