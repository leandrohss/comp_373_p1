package com.facility.dal;

import org.hibernate.Session;

import com.facility.model.maintenance.MaintenanceOrderImpl;

public class MaintenanceScheduleDao {

	public MaintenanceScheduleDao(){}

	public void save(MaintenanceOrderImpl order, int facilityID) {
		
		System.out.println("*************** Saving Maintenance Order in DB with ID ...  " + facilityID);
		
		Session session = HibernatePGSQLHelper.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		session.save(order);
		session.getTransaction().commit();
		
	}
	
}
