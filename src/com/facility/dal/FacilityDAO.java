package com.facility.dal;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;

import com.facility.dal.HibernatePGSQLHelper;

import com.facility.model.facility.Facility;
import com.facility.model.facility.FacilityDetail;

public class FacilityDAO {

	public FacilityDAO(){}

	public List<Facility> findAllFacilities() {
		
		System.out.println("*************** Searcing for all facilities...  ");
		Session session = HibernatePGSQLHelper.getSessionFactory().getCurrentSession();
		session.beginTransaction();
	
        Query getAddresstQuery = session.createQuery("From FacilityImpl");		
		
		System.out.println("*************** Retrieve Query is ....>>\n" + getAddresstQuery.toString()); 
		session.getTransaction().commit();
		
		List<Facility> facilities = getAddresstQuery.list();
		System.out.println("Getting Facailities using HQL. \n");
		
		return facilities;
	}

	
	public int removeFacility(int facilityID) {
			
		System.out.println("*************** Deleteing facility in DB with ID ...  " + facilityID);
		Session session = HibernatePGSQLHelper.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		session.delete(facilityID);
		session.getTransaction().commit();
		
		return 0; //success
	}
	
	
	public int addFacility(Facility facility) {
		
		System.out.println("*************** Adding facility in DB with ID ...  " + facility.getID());
		Session session = HibernatePGSQLHelper.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		session.save(facility);
		session.getTransaction().commit();
		
		return 0; //success
	}
	

	public void addFacilityDetail(FacilityDetail detail) {
		
		System.out.println("*************** Adding facility detail ...");
		Session session = HibernatePGSQLHelper.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		session.save(detail);
		session.getTransaction().commit();
	}
	
	
	public int requestAvailableCapacity(int facilityID) {
		Query getAddresstQuery = null;
		
		try {
		
			System.out.println("*************** Searcing for available capacity from the facility with ID ...  " + facilityID);
			Session session = HibernatePGSQLHelper.getSessionFactory().getCurrentSession();
			session.beginTransaction();
		
	        getAddresstQuery = session.createQuery("From FacilityDetailImpl where facility_id=:facility_id");		
	        getAddresstQuery.setString("customerId", String.valueOf(facilityID));
			
			System.out.println("*************** Retrieve Query is ....>>\n" + getAddresstQuery.toString()); 

		} catch (Exception e) {
			e.printStackTrace();
		}
		return Integer.valueOf(getAddresstQuery.toString());
	}
	
	
	public FacilityDetail getFacilityDetail(int facilityID) {
		List fDetails = null;
		
		try {
			System.out.println("*************** Searcing for facility detail with ID ...  " + facilityID);
			Session session = HibernatePGSQLHelper.getSessionFactory().getCurrentSession();
			session.beginTransaction();
		
	        Query getAddresstQuery = session.createQuery("From FacilityDetailImpl where facility_id=:facility_id");		
	        getAddresstQuery.setString("facility_id", String.valueOf(facilityID));
			
			System.out.println("*************** Retrieve Query is ....>>\n" + getAddresstQuery.toString()); 
			
			fDetails = getAddresstQuery.list();
			System.out.println("Getting Facility Details using HQL. \n" + fDetails.get(0));
			
			System.out.println("*************** Retrieve Query is ....>>\n" + fDetails.get(0).toString()); 
			
			session.getTransaction().commit();
			
			} catch (Exception e) {
				e.printStackTrace();
			}
			return (FacilityDetail) fDetails.get(0);
	}
	
}
