package com.facility.dal;

import java.sql.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.hibernate.Query;
import org.hibernate.Session;

import com.facility.model.maintenance.MaintenanceOrder;

public class MaintenanceOrderDao {

	public MaintenanceOrderDao(){}

	public double getCostForFacility(int facilityID) {
		double cost = 0.0;

		try {
			System.out.println("*************** Searcing for Cost of the facility with ID ...  " + facilityID);
			Session session = HibernatePGSQLHelper.getSessionFactory().getCurrentSession();
			session.beginTransaction();
	        
	        Query getAddresstQuery = session.createQuery("Select SUM(cost) from maintance_order where facility_id=:facility_id");		
	        getAddresstQuery.setString("facility_id", String.valueOf(facilityID));
			
	        cost = (double) getAddresstQuery.list().get(0);
			session.getTransaction().commit();

		} catch (Exception e) {
			e.printStackTrace();
		}

		return cost;
	}
	
	
	public int getDownTimeForFacility(int facilityID) {
		Date repairStart = null;
        Date repairEnd = null;
		
		try {
			System.out.println("*************** Searcing for Down Time of the facility with ID ...  " + facilityID);
			Session session = HibernatePGSQLHelper.getSessionFactory().getCurrentSession();
			session.beginTransaction();
	        
	        Query getAddresstQuery = session.createQuery("Select MO.repairStart, MO.repairEnd from MaintenanceOrderImpl MO where facility_id=:facility_id");		
	        getAddresstQuery.setString("facility_id", String.valueOf(facilityID));
			
	        repairStart = (Date) getAddresstQuery.list().get(0);
	        repairEnd   = (Date) getAddresstQuery.list().get(1);
	        
			session.getTransaction().commit();

		} catch (Exception e) {
			e.printStackTrace();
		}
		
		long duration  = repairEnd.getTime() - repairStart.getTime();
		return (int) TimeUnit.MILLISECONDS.toHours(duration);
	}
	
	
	public List<MaintenanceOrder> findMaintenanceOrdersForFacility(int facilityID) {
		
		System.out.println("*************** Searcing for Use MaintenanceOrders with ID ...  " + facilityID);
		Session session = HibernatePGSQLHelper.getSessionFactory().getCurrentSession();
		session.beginTransaction();
        
		Query getAddresstQuery = session.createQuery("From FacilityUseImpl");		
		
		session.getTransaction().commit();
		
		List<MaintenanceOrder> orders = getAddresstQuery.list();
		System.out.println("Getting Facilities Orders using HQL. \n");
		

		return orders;
	}
	
}
