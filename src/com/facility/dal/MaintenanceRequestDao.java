package com.facility.dal;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;

import com.facility.model.maintenance.MaintenanceRequest;

public class MaintenanceRequestDao {

	public MaintenanceRequestDao(){}

	
	public void save(MaintenanceRequest maintanceRequest, int facilityID) {
		
		System.out.println("*************** Saving maintenance request in DB with ID ...  " + facilityID);
		Session session = HibernatePGSQLHelper.getSessionFactory().getCurrentSession();
		session.beginTransaction();
		session.save(maintanceRequest);
		session.getTransaction().commit();
	}
	
	
	public int getProbleRateForFacility(int facilityID) {
		
		System.out.println("*************** Getting problem rate for the facility with ID ... " + facilityID);
		Session session = HibernatePGSQLHelper.getSessionFactory().getCurrentSession();
		session.beginTransaction();
	
        Query getAddresstQuery1 = session.createQuery("SELECT count From MaintananceRequestImpl where facility_id = " + facilityID);
        
        int count = (int) getAddresstQuery1.list().get(0);
        session.getTransaction().commit();

        return count;
	}
	
	
	public List<MaintenanceRequest> findMaintenanceRequestsForFacility(int facilityID) {
		
		System.out.println("*************** Searcing for Maintenance Requests for the facility with ID ... " + facilityID);
		Session session = HibernatePGSQLHelper.getSessionFactory().getCurrentSession();
		session.beginTransaction();
	
        Query getAddresstQuery = session.createQuery("From MaintenanceResquestImpl");		
		
		System.out.println("*************** Retrieve Query is ....>>\n" + getAddresstQuery.toString()); 
		session.getTransaction().commit();
		
		List<MaintenanceRequest> requests = getAddresstQuery.list();
		System.out.println("Getting Facaility Requests using HQL. \n");
		
		return requests;
	}
	
	
	public List<String> getProblemsForFacility(int facilityID) {
		List<String> problems = null;
		
		try {
			System.out.println("*************** Searcing for Problems of the facility with ID ...  " + facilityID);
			Session session = HibernatePGSQLHelper.getSessionFactory().getCurrentSession();
			session.beginTransaction();
	        
	        Query getAddresstQuery = session.createQuery("Select problem from MaintanenceRequestImpl where facility_id=:facility_id");		
	        getAddresstQuery.setString("facility_id", String.valueOf(facilityID));
			
	        problems = getAddresstQuery.list();
			session.getTransaction().commit();

		} catch (Exception e) {
			e.printStackTrace();
		}

		return problems;
	}
	
}
